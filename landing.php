<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta property="og:type" content="website" />
<meta property="og:image" content="assets/img/demo.jpg" />
<meta charset="utf-8">
<meta property="og:url" content="" />
<meta property="og:title" content="Internet Marketing Software" />
<meta property="og:description" content="Provide solutions for internet marketing" />
<title>Fptultimate.com - Internet Marketing Software</title>
<link  rel="icon" href="assets/img/favicon.ico">
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/css/font-awesome.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css?family=Fira+Sans" rel="stylesheet">
<link rel="stylesheet" href="assets/css/app.css">
<script type="text/javascript" src="assets/js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>

</head>

<body>
    <header class="navbar navbar-default navbar-fixed-top header">
     <div class="container" >
       <div class="navbar-header"> 
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button> 
        <a class="navbar-brand logo" itemprop="headline" href=""><img height="30" src="https://autoengage.pro/assets/img/logo.png" /></a>
      </div>
      
      <nav class="collapse navbar-collapse top-menu" role="navigation" id="navbar">
         <div class="navbar-right">
            <ul style="padding-left:10px">
                 <li style="padding-top:10px;padding-right:15px;list-style:none;"> 
                    <a class="hidden-sm hidden-xs btn btn-warning btn-df" href="user/profile">Start Free Trial</a> 
                    <a class="hidden-lg hidden-md btn btn-warning btn-df" href="user/profile">Contact us</a> 
                </li>
            </ul>
         </div>
        
      </nav>
     </div>	
    </header>

	<div class="wrapper">

      	<div class="page_1">
    		<div class="container main-content">
                <h1><b>Auto Engage : </b> A Simple Tool</h1>
                <div style="display:inline-block;border:10px solid #333;border-radius:5px;">
    			<iframe width="854" height="480" src="https://www.youtube.com/embed/bzdleWIcLHQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
               </div> 
               <p class="page_1-footer">Let Me Show You How Simple It Is To Create & Edit Pages Inside Of ClickFunnels...</p>
            </div>
        </div>

        <div class="page page_2">
            <div class="container">
                <div class="step-1 col-md-4 col-sm-12">
                    <h1 class="step-titel">Step #1</h1>
                    <div class="step-line"></div>
                    <p class="step-content">Pick Your Favorite Design From Our</p>
                    <p class="step-description"> Inside of ClickFunnels we have dozens of amazing templates that are scientifically proven to convert that you can use for free inside of your account!</p>
                </div>
                <div class="col-md-8 col-sm-12">
                    <img class="img-responsive" src="assets/img/page1_1.png">
                </div>
            </div>
        </div>

         <div class="page page_3">
            <div class="container">
                <div class="col-md-8 col-sm-12">
                    <img class="img-responsive" src="assets/img/page1_2.png">
                </div>

                 <div class="step-2 col-md-4 col-sm-12">
                    <h1 class="step-titel">Step #2</h1>
                    <div class="step-line"></div>
                    <p class="step-content">Replace each of the elements</p>
                    <p class="step-description">Yes, you can use our proven templates as a model to sell your products!  Just quickly replace the logos, videos, products and text with your own</p>
                </div>
            </div>
        </div>

        <div class="page page_4">
            <div class="container">
                <div class="step-3 col-md-4 col-sm-12">
                    <h1 class="step-titel">Step #3</h1>
                    <div class="step-line"></div>
                    <p class="step-content">Drag and drop over 50 proven sales elements instantly onto your page!</p>
                    <p class="step-description">If you can drag and drop, then you have the skills you need to make the pages inside of your funnel convert!  The simplicity of our editor makes it so ANYONE can build the sales funnel of their dreams!</p>
                </div>
                <div class="col-md-8 col-sm-12">
                    <img class="img-responsive" src="assets/img/page1_3.png">
                </div>

                 
            </div>
        </div>

         <div class="page page_5">
            <div class="container">
                <div class="col-md-8 col-sm-12">
                    <img class="img-responsive" src="assets/img/page1_2.png">
                </div>

                 <div class="step-2 col-md-4 col-sm-12">
                    <h1 class="step-titel">Step #4</h1>
                    <div class="step-line"></div>
                    <p class="step-content">Replace each of the elements</p>
                    <p class="step-description">Yes, you can use our proven templates as a model to sell your products!  Just quickly replace the logos, videos, products and text with your own</p>
                </div>
            </div>
        </div>

        <div class="page page_6">
            <div class="container">
                <div class="step-3 col-md-4 col-sm-12">
                    <h1 class="step-titel">Step #5</h1>
                    <div class="step-line"></div>
                    <p class="step-content">Drag and drop over 50 proven sales elements instantly onto your page!</p>
                    <p class="step-description">If you can drag and drop, then you have the skills you need to make the pages inside of your funnel convert!  The simplicity of our editor makes it so ANYONE can build the sales funnel of their dreams!</p>
                </div>
                <div class="col-md-8 col-sm-12">
                    <img class="img-responsive" src="assets/img/page1_3.png">
                </div>

                 
            </div>
        </div>

        <div class="page page_7">
            <div class="container">
                <div class="col-md-8 col-sm-12">
                    <img class="img-responsive" src="assets/img/page1_2.png">
                </div>

                 <div class="step-2 col-md-4 col-sm-12">
                    <h1 class="step-titel">Step #6</h1>
                    <div class="step-line"></div>
                    <p class="step-content">Replace each of the elements</p>
                    <p class="step-description">Yes, you can use our proven templates as a model to sell your products!  Just quickly replace the logos, videos, products and text with your own</p>
                </div>
            </div>
        </div>

        <div class="page page_8">
            <div class="container">
                <div class="step-3 col-md-4 col-sm-12">
                    <h1 class="step-titel">Step #7</h1>
                    <div class="step-line"></div>
                    <p class="step-content">Drag and drop over 50 proven sales elements instantly onto your page!</p>
                    <p class="step-description">If you can drag and drop, then you have the skills you need to make the pages inside of your funnel convert!  The simplicity of our editor makes it so ANYONE can build the sales funnel of their dreams!</p>
                </div>
                <div class="col-md-8 col-sm-12">
                    <img class="img-responsive" src="assets/img/page1_3.png">
                </div>

                 
            </div>
        </div>
         <div class="page page_9">
            <div class="container">
                <div class="col-md-8 col-sm-12">
                    <img class="img-responsive" src="assets/img/11.png">
                </div>
                <div class="step-3 col-md-4 col-sm-12">
                    <h1 class="step-titel">Step #8</h1>
                    <div class="step-line"></div>
                    <p class="step-content">Start your FREE 14 day trial to ClickFunnels now...</p>
                    <p class="step-description">Yes, that means you can get your own ClickFunnels account today for FREE, and play with everything for the next 14 days, and put us the test.</p>
                    <a href="#" class="btn btn-success btn-trial">
                        <p><i class="fa fa-arrow-right"></i> Start Your Free 3 Day Trial </p>
                        <span>Sign-up Takes Less Than 60 Seconds.</span>
                    </a>
                </div>
                 
            </div>
        </div>

    </div><!--End Wrap -->
    
	<footer id="footer" >
        <div class="container">
            <div class="col-md-3 sub-footer">
                <b><p>ABOUT</p></b>
                <a href="#"><p>About Our</p></a>
                <a href="#"><p>Terms of Service</p></a>
                <a href="#"><p>Privacy</p></a>
            </div>
            <div class="col-md-3 sub-footer">
                <b><p>PRODUCTS</p></b>
                <a target="_blank" href="https://adssuccess.com"><p>Adsuccess</p></a>
                <a target="_blank" href="https://kingtarget.pro"><p>Kingtarget</p></a>
                <a target="_blank" href="https://fanpagebuilder.pro"><p>FanpageBuilder</p></a>
                <a target="_blank" href="https://autoengage.com"><p>Auto Engage</p></a>
            </div>
            <div class="col-md-3 sub-footer">
                <b><p>HELP</p></b>
                <a target="_blank" href="https://fptultimate.com"><p>Home</p></a>
                <a target="_blank" href="https://www.facebook.com/messages/t/nguyenvuteam"><p>Messenger</p></a>
                <a target="_blank" href="#"><p>Group Facebook</p></a>
            </div>
            <div class="col-md-3 sub-footer">
                <img class="img-responsive" src="https://autoengage.pro/assets/img/logo.png" height="15" width="180">
            </div>
            <div class="col-md-12 copy-right">
                &copy; Fptultimate.com, All Rights Reserved
            </div>
        </div>
	</footer>

</body>
</html>
    